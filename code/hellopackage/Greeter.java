package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
public class Greeter{
    public static void main(String[]args){
       Scanner scan = new Scanner(System.in);
       Random rand = new Random(0);
       Utilities doubler = new Utilities();
       System.out.println("Enter a number.");
       int numEntered = scan.nextInt();
       numEntered=doubler.doubleMe(numEntered);
       System.out.println("Heres your number doubled. "+numEntered);
    }
}